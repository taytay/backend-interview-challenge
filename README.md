# WaveLynx Backend Interview Challenge

This is a quick and dirty challenge for testing backend software
dev skills.

---

## Instructions

* Create a **Python Flask** REST API which runs on a local machine.
* The API should expose the REST endpoints on port **8080**.
* The Endpoints should follow the specification below.
* Once completed, archive your project and return the source code to Wavelynx.

---

## API Specification

* The following API endpoints should be exposed via **localhost** on port **8080**.
* All endpoints should support JSON payloads through the `Content-Type: application/json` header.
* Sane response codes should be returned for requests that are successful or return errors.

### Get Token

This endpoint gets a randomized token from of specified string length.

|            |          |
|------------|----------|
| **Path**   | `/token` |
| **Method** | `GET`    |

#### Params

| **Key**  | **Description**                                                                   |
|----------|-----------------------------------------------------------------------------------|
| `length` | Requested length of returned token, should only allow values between `8` and `32` |

#### Request

N/A

#### Response

The response should contain a random token of the requested length.

```
{
    "token": "123456...",
    "status": "success"
}
```

### Verify Token

The verify token endpoint should take in a token from the request payload and check if it had
previously been issued via the **Get Token** endpoint. If it has, then a success response should
be returned. Otherwise an appropriate error should be given.

|            |           |
|------------|-----------|
| **Path**   | `/verify` |
| **Method** | `POST`    |

#### Params

N/A

#### Request

The request should contain a valid token previously returned from the **Get Token** request.

```
{
    "token": "123456..."
}
```

#### Response

The response should contain an appropriate status message if the token was previously issued or not.

```
{
    "status": "{MESSAGE}"
}
```
